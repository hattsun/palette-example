package com.ushi.example.palette.fragment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ushi.example.palette.R;
import com.ushi.example.palette.databinding.FragmentPaletteBinding;
import com.ushi.example.palette.util.Log;

import java.io.IOException;

/**
 * Created by hatsuno.daisuke on 2016/08/19.
 */
public class PaletteFragment extends BaseFragment {

    public static final int REQUEST_PICK_IMAGE = 0;

    private FragmentPaletteBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_palette, container, false);

        mBinding.setClickImagePick(view -> {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");

//            intent = Intent.createChooser(intent, "イメージ選択");

            startActivityForResult(intent, REQUEST_PICK_IMAGE);
        });

        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (uri == null) {
                Log.e();
                return;
            }

            load(uri);
        }
    }

    private void load(Bitmap bitmap) {
        mBinding.image.setImageBitmap(bitmap);
        mBinding.setPalette(Palette.from(bitmap).generate());
    }

    private void load(Uri uri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);
            load(bitmap);

        } catch (IOException e) {
            Log.e(e);
        }
    }
}
