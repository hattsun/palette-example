package com.ushi.example.palette.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

/**
 * Created by hatsuno.daisuke on 2016/05/16.
 */
public abstract class BaseFragment extends Fragment {

    private boolean hasTitle;

    public BaseFragment() {
        setArguments(new Bundle());
    }

    public boolean dispatchBackPressed() {
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        invalidateTitle();
    }

    /**
     * @see #getTitle(Resources)
     */
    protected void setHasTitle(boolean hasTitle) {
        if (this.hasTitle == hasTitle) return;

        this.hasTitle = hasTitle;
        invalidateTitle();
    }

    /**
     * @see #setHasTitle(boolean)
     */
    protected String getTitle(Resources res) {
        return null;
    }

    protected void invalidateTitle() {
        if (!hasTitle) return;

        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setTitle(getTitle(getResources()));
        }
    }
}
