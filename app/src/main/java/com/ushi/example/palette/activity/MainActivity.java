package com.ushi.example.palette.activity;

import android.os.Bundle;

import com.ushi.example.palette.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
