package com.ushi.example.palette.util;


/**
 * android.util.Log のラッパー
 *
 * @author hatsuno.daisuke
 */
public class Log {

    private static final StackHelper HELPER = new StackHelper(Log.class,
            "dalvik.system.VMStack", "java.lang.Thread");

    /**
     * {@link android.util.Log#i(String, String)}
     *
     * @param msg message
     */
    public static void i(String msg) {
        android.util.Log.i(getClassName(), createMessage(msg));
    }

    /**
     * {@link android.util.Log#i(String, String)}
     *
     * @param ob Object
     */
    public static void i(Object ob) {
        android.util.Log.i(getClassName(), createMessage(ob));
    }

    /**
     * {@link android.util.Log#i(String, String)}
     * 現在のソースコード上の位置を出力する.
     */
    public static void i() {
        android.util.Log.i(getClassName(), getMethodName());
    }

    /**
     * {@link android.util.Log#d(String, String)}
     *
     * @param msg message
     */
    public static void d(String msg) {
        android.util.Log.d(getClassName(), createMessage(msg));
    }

    /**
     * {@link android.util.Log#d(String, String)}
     * 現在のソースコード上の位置を出力する.
     */
    public static void d() {
        android.util.Log.d(getClassName(), getMethodName());
    }

    /**
     * {@link android.util.Log#d(String, String)}
     *
     * @param ob Object
     */
    public static void d(Object ob) {
        android.util.Log.d(getClassName(), createMessage(ob));
    }

    /**
     * {@link android.util.Log#wtf(String, String)}
     *
     * @param msg message
     */
    public static void wtf(String msg) {
        android.util.Log.wtf(getClassName(), createMessage(msg));
    }

    /**
     * {@link android.util.Log#wtf(String, String)}
     *
     * @param ob Object
     */
    public static void wtf(Object ob) {
        android.util.Log.wtf(getClassName(), createMessage(ob));
    }

    /**
     * {@link android.util.Log#w(String, String, Throwable)}
     *
     * @param th Throwable
     */
    public static void w(Throwable th) {
        android.util.Log.w(getClassName(), "Exception raised.", th);
    }

    /**
     * {@link android.util.Log#w(String, String, Throwable)}
     *
     * @param msg message
     * @param th  Throwable
     */
    public static void w(String msg, Throwable th) {
        android.util.Log.w(getClassName(), createMessage(msg), th);
    }

    /**
     * {@link android.util.Log#w(String, String, Throwable)}
     *
     * @param ob Object
     * @param th Throwable
     */
    public static void w(Object ob, Throwable th) {
        android.util.Log.w(getClassName(), createMessage(ob), th);
    }

    /**
     * {@link android.util.Log#w(String, String)}
     *
     * @param msg message
     */
    public static void w(String msg) {
        android.util.Log.w(getClassName(), createMessage(msg));
    }

    /**
     * {@link android.util.Log#w(String, String)}
     *
     * @param ob Object
     */
    public static void w(Object ob) {
        android.util.Log.w(getClassName(), createMessage(ob));
    }

    /**
     * {@link android.util.Log#w(String, String)}
     * 現在のソースコード上の位置を出力する.
     */
    public static void w() {
        android.util.Log.w(getClassName(), getMethodName());
    }

    /**
     * {@link android.util.Log#d(String, String)}
     *
     * @param msg message
     */
    public static void e(String msg) {
        android.util.Log.e(getClassName(), createMessage(msg));
    }

    /**
     * {@link android.util.Log#d(String, String)}
     *
     * @param ob Object
     */
    public static void e(Object ob) {
        android.util.Log.e(getClassName(), createMessage(ob));
    }

    /**
     * {@link android.util.Log#e(String, String, Throwable)}
     *
     * @param th Throwable
     */
    public static void e(Throwable th) {
        android.util.Log.e(getClassName(), "Exception occured.", th);
    }

    /**
     * {@link android.util.Log#e(String, String, Throwable)}
     *
     * @param msg message
     * @param th  Throwable
     */
    public static void e(String msg, Throwable th) {
        android.util.Log.e(getClassName(), createMessage(msg), th);
    }

    /**
     * {@link android.util.Log#e(String, String, Throwable)}
     *
     * @param ob Object
     * @param th Throwable
     */
    public static void e(Object ob, Throwable th) {
        android.util.Log.e(getClassName(), createMessage(ob), th);
    }

    /**
     * {@link android.util.Log#e(String, String)}
     * 現在のソースコード上の位置を出力する.
     */
    public static void e() {
        android.util.Log.e(getClassName(), getMethodName());
    }

    static String createMessage(Object ob) {
        return ob != null ? ob.toString() : "null";
    }

    static String getClassName() {
        return getClassName(0);
    }

    static String getClassName(int ignoreDepth) {
        return HELPER.getSimpleName(ignoreDepth);
    }

    static String getMethodName() {
        return getMethodName(0);
    }

    static String getMethodName(int ignoreDepth) {
        return HELPER.getMethodName(ignoreDepth);
    }
}